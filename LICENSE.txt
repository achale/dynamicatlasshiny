This Dynamic Atlas package as a whole is distributed under the MIT License.

Copyright 2017 Dr Alison Hale, Lancaster University, UK.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


This package includes other open source software components. The following is a list of these components (full copies of the 
license agreements used by these components can be found on their respective websites):

- leaflet-ajax, https://github.com/calvinmetcalf/leaflet-ajax
- Chart.js, http://www.chartjs.org/
- Chroma.js, https://github.com/gka/chroma.js
- Leaflet.GridLayer.GoogleMutant, https://gitlab.com/IvanSanchez/Leaflet.GridLayer.GoogleMutant
- Leaflet, http://leafletjs.com
- ES6-Promise, https://github.com/stefanpenner/es6-promise


