# A Shiny framework for the Dynamic Atlas

The Dynamic Atlas software is a web-based application designed to display geospatial data which changes over time.  This application can either work as a stand-alone application or can be embedded in the Shiny framework.


The **Stand-alone** version is available from https://gitlab.com/achale/dynamicatlas and there is a demo at https://achale.gitlab.io/dynamicatlas/. It is supported by a tutorial which can be accessed at https://achale.gitlab.io/dynamicatlastutorial/ and downloaded from https://gitlab.com/achale/dynamicatlastutorial/.


## Shiny framework
The files in this repository provide the Shiny framework in which the Dynamic Atlas can be embedded.  To use this Shiny framework you will need to put the files on a Shiny server.  A working demo is available at http://fhm-chicas-apps.lancs.ac.uk/shiny/users/haleac/healthatlas/.


**Setup:**

1) Download the files in this repository to your Shiny server.
2) Download the files from https://gitlab.com/achale/dynamicatlas and put them in the 'www' folder.
3) The required folder structure is shown below<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;root<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---www<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---apps<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---css<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---data<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---dataexample<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---images<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---img<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---js<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---help.html<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---index.html<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---indexShiny.html<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---info.html<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---app.R<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---LICENSE.txt<br>
4) The following files can be deleted www/index.html, www/info.html, www/data/\*.\*, www/js/allMetadata.js.
5) This Shiny framework for the Dynamic Atlas uses Google Maps by default.  If you have a Google Maps key edit app.R by replacing the text *PutYourKeyHere* with your key.  If you don't have a key the software will still work; see further instructions in step 6) below.
6) Open the Shiny website on your server. Check it is working by following the on-screen instructions and using the example data. If you have not added a Google Maps key to app.R then in the "Import metadata" section check the "filling in 'long' form" radio button, after which under "Global options" set *userDefinedBaseMaps=true*, finally scroll to the bottom of the screen and click "make map"; this forces the software to use OpenStreetMap instead of Google Maps.

If you prefer not to use Google Maps then take note of the following.  When using the example data the radio button options "uploading file" and "filling in 'short' form" both assume you have added your Google Maps key to app.R; if you have not added your key there will be no base layer map.  If you have made your own metadata in which *userDefinedBaseMaps=true* then uploading it using "uploading file" (in "Import metadata" Section) will force the Dynamic Atlas to use OpenStreetMap hence app.R does not require the key. Conversely using "filling in 'short' form" always uses Google Maps so this option requires the key in app.R.



